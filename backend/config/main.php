<?php
return [
    'id'                  => 'app-backend',
    'homeUrl'             => '/adkot/',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => ['log'],
    'defaultRoute'        => 'admin',
    'components'          => [
        'request'      => [
            'baseUrl' => '/admin',
        ],
        'formatter'    => [
            'class' => 'yii\i18n\Formatter',

        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'admin/site/error',
        ],
        'view'         => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-advanced-app'
                ],
            ],
        ],
    ]
];
