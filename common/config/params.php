<?php
return [
    'adminEmail'                    => 'seoznik@yandex.ru',
    'supportEmail'                  => 'seoznik@yandex.ru',
    'user.passwordResetTokenExpire' => 3600,
    'adminUrl'                      => '/adkot',
];
