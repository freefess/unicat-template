<?php
$params = array_merge(require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php'));
return [
    'vendorPath'     => dirname(dirname(__DIR__)) . '/vendor',
    'language'       => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'components'     => [
        'cache'        => [
            'class' => 'yii\caching\FileCache',
        ],
        'assetManager' => [
            'forceCopy' => YII_DEBUG,
        ],
        'mailer'       => [
            'class'            => 'yii\swiftmailer\Mailer',
            'viewPath'         => '@common/mail',
            'useFileTransport' => false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => 'smtp.yandex.ru',
                'username'   => 'foursite.sws@yandex.ru',
                'password'   => 'Demonkot77',
                'port'       => '587',
                'encryption' => 'TLS',
            ],
        ],
        'reCaptcha'     => [
            'name'    => 'reCaptcha',
            'class'   => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LeLawgUAAAAAA9H97WcdtjlKzJTz41GZUYPGb_t',
            'secret'  => '6LeLawgUAAAAANb-Cq0gZPodNQVQp5lxj_-eux6x',
        ],
    ],
    'params'         => $params,
];
