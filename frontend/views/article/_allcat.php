<?php
use yii\helpers\Html;

/**
 * @var \freefess\unicat\models\ArticleCategory $model
 */
?>
<div class="content_element">
    <h3 class="content_element_header">
        <? echo Html::a(Html::encode($model->name . "(Статей - )"),
            $model->selfUrl()); ?>
    </h3>

    <div class="content_element_body"></div>
</div>
