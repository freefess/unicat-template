<?php
use yii\helpers\Html;

/**
 * @var \freefess\unicat\models\Article $model
 */
?>
<div class="content_element">
    <h3 class="content_element_header main-center-header">
        <? echo Html::a(Html::encode($model->name),
            $model->selfUrl()); ?>
    </h3>

    <div class="content_element_body"><?= $model->shortContent(200) ?></div>
</div>