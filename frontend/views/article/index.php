<?php
/** @var $this yii\web\View
 * @var freefess\unicat\models\Object $object
 * @var freefess\unicat\models\Item[] $allModel
 */
?>
<div class="body-content row-fluid">
    <div class="col-md-14">
        <h1>Каталог саун Воронежа</h1>
    </div>
    <div class="col-md-14">
        <div class="sort-item">
            <div class="sort-item-inner sort-item-header">Сортировать</div>
            <div class="sort-item-inner">По популярности</div>
            <div class="sort-item-inner">По цене</div>
            <div class="sort-item-inner sort-item-end">По рейтингу</div>
            <div class="clearfix"></div>
        </div>
    </div>

    <?php
    if ($allModel) {
        foreach ($allModel as $key => $model) {
            if ($key == 2) {
                echo $this->render('_map',
                    [
                        'allModel' => $allModel,
                        'model'    => $model,
                    ]);
            }
            echo $this->render('_sauna',
                [
                    'model' => $model,
                ]);
        }
        if (count($allModel) < 3) {
            echo $this->render('_map',
                [
                    'allModel' => $allModel,
                    'model'    => $model,
                ]);
        }
    } else {
        echo "Объекты отсутствуют";
    }
    ?>
    <div class="col-md-14 description">
        <?= (isset($object->seo->description)) ? $object->seo->description : '' ?>
    </div>
</div>

