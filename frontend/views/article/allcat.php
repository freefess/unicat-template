<?php
/**
 * @var CActiveDataProvider $dataProvider
 */

?>
<div class="body-content row-fluid">
        <h1 class="name_org">Все статьи</h1>

        <div class="inner_content">
            <?php
            echo \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_allcat',
            ]);
            ?>

        </div>
</div>
