<?php

/**
 * @var freefess\unicat\models\Article $model
 * @var freefess\unicat\models\Item[] $similarObject
 */

?>

<div class="body-content container-fluid">

    <div class="row ">
        <div class="col-md-12">
            <h1 class="name-article"><?= $model->name ?></h1>

            <div class="content"><?= $model->content ?></div>
        </div>
    </div>
</div>
