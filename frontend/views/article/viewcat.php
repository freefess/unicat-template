<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \common\models\ArticleCategory $model
 */

?>
<div class="body-content row-fluid">
    <div class="col-md-12">
        <h1 class="name-article"><?= $model->name ?></h1>

        <div class="inner_content">
            <?php
            echo \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView'     => '_viewcat',
            ]);
            ?>

        </div>
    </div>
</div>
