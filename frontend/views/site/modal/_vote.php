<?php

use freefess\unicat\models\Vote;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model freefess\unicat\models\Vote */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-header">
    <h3>Оставить отзыв</h3>
    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
</div>
<div class="modal-body">

    <?php Pjax::begin(['id' => 'vote-pjax', 'enablePushState' => false]) ?>
    <?php $form = ActiveForm::begin([
        'options' => ['data-pjax' => true]
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vote')->radioList(Vote::listVoteValues()) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className(), [
        'siteKey'       => Yii::$app->reCaptcha->siteKey,
        'widgetOptions' => [
            'id' => 're-captcha-vote',
        ]
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

</div>
