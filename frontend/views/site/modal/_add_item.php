<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model freefess\unicat\models\Vote */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-header">
    <h3>Добавить сауну</h3>
    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
</div>
<div class="modal-body">

    <?php Pjax::begin(['id' => 'add-item-pjax', 'enablePushState' => false]) ?>
    <?php $form = ActiveForm::begin([
        'options' => ['data-pjax' => true]
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'contact')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className(),
        ['siteKey' => Yii::$app->reCaptcha->siteKey,
         'widgetOptions' => [
             'id' => 're-captcha-add-item',
         ]]) ?>


    <div class="form-group">
        <?= Html::submitButton('Добавить',
            ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

</div>
