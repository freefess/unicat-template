<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/**
 * @var freefess\unicat\models\Object[] $allModel
 */
?>
<div class="site-index">


    <div class="body-content">
        <?php
        foreach ($allModel as $model) {
            echo Html::a($model->name, Url::to(["/object/index", 'controller' => $model->server_name])) . "<br>";
        }
        ?>
    </div>

</div>

