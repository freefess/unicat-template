<?php

/* @var $this \yii\web\View */
/* @var $content string */

use freefess\unicat\widgets\Alert;
use freefess\unicat\widgets\banner\BannerWidget;
use freefess\unicat\widgets\filter\FilterWidget;
use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="container">
    <div class="row header">
        <div class="right-container-wrap">
            <div class="right-container">
                <div class="top-banner">
                    <?= BannerWidget::widget([
                        'serverName' => 'top-banner',
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="left-container">
            <a href="/" id="logo">
                <?= \freefess\unicat\widgets\snippet\SnippetWidget::widget([
                    'serverName' => 'top-phone',
                ]); ?>
            </a>
        </div>
    </div>
</div>
<div class="top-menu">
    <div class="container">
        <div class="row top-menu-body">
            <div class="right-container-wrap">
                <div class="right-container">
                    <?= \yii\widgets\Menu::widget([
                        'options'         => [
                            'id' => 'top-center',
                        ],
                        'submenuTemplate' => "\n<ul class='sub-menu'>\n{items}\n</ul>\n",
                        'items'           => \freefess\unicat\models\Menu::getLinks('top-center')
                    ]) ?>
                </div>
            </div>
            <div class="left-container">
                <?php
                \yii\bootstrap\Modal::begin([
                    'id'            => 'add-item-modal',
                    'toggleButton'  => [
                        'label'       => 'Добавить сауну',
                        'tag'         => 'a',
                        'data-target' => '#add-item-modal',
                        'data-remote' => \yii\helpers\Url::toRoute([
                            'site/add-item'
                        ]),
                        'href'        => '#add-item',
                        'class'       => 'add-item'
                    ],
                    'clientOptions' => false,
                ]);
                \yii\bootstrap\Modal::end();
                ?>
            </div>
        </div>
    </div>
</div>

<div class="container container-content">
    <div class="row">

        <div class="right-container-wrap">
            <div class="right-container">
                <?= Breadcrumbs::widget([
                    'homeLink' => ['label' => 'Все сауны Воронежа', 'url' => '/'],
                    'links'    => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
        <div class="left-container">
            <div id="filter">
                <div class="filter-head">
                    <span class="active">Фильтры</span>
                    <span>Каталог</span>

                    <div class="clearfix"></div>
                </div>
                <div class="filter-body">
                    <? //$this->getGlobalWidget('name') ?>
                    <?= FilterWidget::widget([
                        'object'      => Yii::$app->controller->object,
                        'filterParam' => Yii::$app->request->get('f')
                    ]); ?>
                </div>
            </div>
            <div class="banner-left">
                <?= BannerWidget::widget([
                    'serverName' => 'banner-left-1',
                ]); ?>
            </div>
            <div class="banner-left">
                <?= BannerWidget::widget([
                    'serverName' => 'banner-left-2',
                ]); ?>
            </div>
            <div class="banner-left">
                <?= BannerWidget::widget([
                    'serverName' => 'banner-left-3',
                ]); ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container footer-content">
        <div class="row">
            <div class="col-xs-12 content col-md-7 col-md-push-2">
                <div class="row-fluid">
                    <div class="content col-xs-4 col-md-4">
                        <div class="footer-menu-header">
                            Аналитика
                        </div>
                        <div class="footer-menu-body">
                            <?= \yii\widgets\Menu::widget([
                                'items' => [
                                    ['label' => 'Главная', 'url' => ['/']],
                                    ['label' => 'О компании', 'url' => ['/']],
                                    ['label' => 'Бани', 'url' => ['/']],
                                    ['label' => 'Интересное', 'url' => ['/']],
                                    ['label' => 'О проекте', 'url' => ['/']],
                                ]
                            ]) ?>
                        </div>
                    </div>
                    <div class="content col-xs-4 col-md-4">
                        <div class="footer-menu-header">
                            Бизнесмену
                        </div>
                        <div class="footer-menu-body">
                            <?= \yii\widgets\Menu::widget([
                                'items' => [
                                    ['label' => 'Главная', 'url' => ['/']],
                                    ['label' => 'О компании', 'url' => ['/']],
                                    ['label' => 'Бани', 'url' => ['/']],
                                    ['label' => 'Интересное', 'url' => ['/']],
                                    ['label' => 'О проекте', 'url' => ['/']],
                                ]
                            ]) ?>
                        </div>
                    </div>
                    <div class="content col-xs-4 col-md-4">
                        <div class="footer-menu-header">Куда пойти</div>
                        <div class="footer-menu-body">
                            <?= \yii\widgets\Menu::widget([
                                'items' => [
                                    ['label' => 'Главная', 'url' => ['/']],
                                    ['label' => 'О компании', 'url' => ['/']],
                                    ['label' => 'Бани', 'url' => ['/']],
                                    ['label' => 'Интересное', 'url' => ['/']],
                                    ['label' => 'О проекте', 'url' => ['/']],
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-2 col-md-pull-7 footer-side-col footer-left-menu">
                <?= \yii\widgets\Menu::widget([
                    'items' => \freefess\unicat\models\Menu::getLinks('left-bottom')
                ]) ?>
            </div>
            <div class="col-xs-6  col-md-3 footer-side-col footer-right-col">
                <a href="/" class="footer-logo"></a>
                <span class="footer-right-copy">sauna-voronezh.ru 2016</span>
                <span class="footer-right-name">Сауна Воронеж, официальный сайт</span>
                Все права защищены
                <div class="footer-right-informer">
                    <!-- Yandex.Metrika informer -->
                    <a href="https://metrika.yandex.ru/stat/?id=36739240&amp;from=informer"
                       target="_blank" rel="nofollow"><img
                            src="http://informer.yandex.ru/informer/36739240/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                            style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика"
                            title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"
                            onclick="try{Ya.Metrika.informer({i:this,id:36739240,lang:'ru'});return false}catch(e){}"/></a>
                    <!-- /Yandex.Metrika informer -->
                </div>

                <!-- Yandex.Metrika counter -->
                <script type="text/javascript">
                    (function (d, w, c) {
                        (w[c] = w[c] || []).push(function () {
                            try {
                                w.yaCounter36739240 = new Ya.Metrika({
                                    id: 36739240,
                                    clickmap: true,
                                    trackLinks: true,
                                    accurateTrackBounce: true,
                                    webvisor: true,
                                    ut: "noindex"
                                });
                            } catch (e) {
                            }
                        });

                        var n = d.getElementsByTagName("script")[0],
                            s = d.createElement("script"),
                            f = function () {
                                n.parentNode.insertBefore(s, n);
                            };
                        s.type = "text/javascript";
                        s.async = true;
                        s.src = "https://mc.yandex.ru/metrika/watch.js";

                        if (w.opera == "[object Opera]") {
                            d.addEventListener("DOMContentLoaded", f, false);
                        } else {
                            f();
                        }
                    })(document, window, "yandex_metrika_callbacks");
                </script>
                <noscript>
                    <div><img src="https://mc.yandex.ru/watch/10858228?ut=noindex"
                              style="position:absolute; left:-9999px;" alt=""/></div>
                </noscript>
                <!-- /Yandex.Metrika counter -->
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
