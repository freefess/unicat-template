<?php

return [
    'id'                  => 'app-practical-frontend',
    'basePath'            => dirname(__DIR__),
    'homeUrl'             => '/',
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components'          => [
        'view' => [
            'class' => 'freefess\unicat\components\ExtendView',
        ],

        'yandexMapsApi' => [
            'class' => 'katzz0\yandexmaps\Api',
        ],
        'log'           => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler'  => [
            'errorAction' => 'site/error',
        ],
        'urlManager'    => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'baseUrl'         => "/",
            'suffix'          => '/',
            'rules'           => require(__DIR__ . '/rules.php'),
        ],

    ]
];
