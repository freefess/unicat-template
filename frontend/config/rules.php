<?php
return [
    [
        'class' => 'frontend\components\FirstUrlRule',
    ],
    //'<controller:\w+>/'=>'model/index',
    'object/ajax-table'                    => 'object/ajax-table',
    'sale/'                                => 'site/sale',
    'type/<url>/'                          => 'seo/index',
    'site/<controller:.*?>'                => 'site/<controller>',
    '<controller:[\w]+>/ajax-filter-count' => 'object/ajax-filter-count',
    '<controller:[\w]+>/<slug:.+>'         => 'object/view',
    '<controller:\w+>'                     => 'object/index',
];
